import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from "react-router-dom";
import logo from '../../images/logo.png';

import './header.scss';

export default class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isExpanded: false
        }

        this.handleSearch = this.handleSearch.bind(this);
        this.handleMenu = this.handleMenu.bind(this);
    }

    handleSearch(e) {
        this.setState({
            isExpanded: !this.state.isExpanded
        });
    }

    handleMenu(e) {
        this.setState({
            isExpanded: !this.state.isExpanded
        });
    }

    render() {
        let headerClass = this.state.isExpanded ? 'expanded' : 'collapsed';

        return (
            <div className={headerClass}>
                <header className='header'>
                    <div className='logo'>
                        <Link to='/' >
                            <img src={logo} alt='logo'/>
                        </Link>
                    </div>
                    <div className='search'>
                        <FontAwesomeIcon
                            icon={'search'}
                            size={'2x'}
                            color={'#0d0d0d'}
                            onClick={this.handleSearch}
                        />
                    </div>
                    <nav className='nav'>
                        <FontAwesomeIcon
                            icon={'list'}
                            size={'3x'}
                            color={'#0d0d0d'}
                            onClick={this.handleMenu}
                        />
                    </nav>
                </header>
                <div className='header-expanded'>
{/*                    {this.state.isExpanded ? <label htmlFor='search'>
                        <input id='search' placeholder=''/>
                    </label> : null}*/}
                </div>
            </div>

        )
    }
}