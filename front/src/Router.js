import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './Containers/Home/Home';
import Articles from './Containers/Articles/Articles';

export default () =>
    <Switch>
        <Route exact path="/" component={Home}/>
        <Route path="/articles" component={Articles}/>
    </Switch>
