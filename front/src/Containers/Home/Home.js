import React, { Component } from 'react';
import { Link } from "react-router-dom";
import Header from '../../Components/Header/Header';

import './home.scss';

class Home extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return(
            <div>
                <Header />
                <Link to="/articles">Articles</Link> <br/>
            </div>
        )
    }
}

export default Home;

