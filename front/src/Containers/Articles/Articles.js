import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as articlesActions from '../../actions/articles';

class Article extends Component {
    constructor(props){
        super(props);
    }

    componentWillMount() {
        console.log('compo will mount');
        this.props.fetchArticles();
    }

    render() {
        console.log('props = ', this.props)
        const { articles } = this.props;

        return(
            <div>
                Articles
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { articles: { list } } = state;
    return {
        article: list
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchArticles: bindActionCreators(articlesActions.fetchArticles, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Article);

