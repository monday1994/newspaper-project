import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import Store from './store';
import initialStoreState from './tempInitialState';
const StoreInstance = Store(initialStoreState);

//importing libs
require('./externalLibs/fontawesome5');


ReactDOM.render(
    <Provider store={StoreInstance}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>, document.getElementById('root'));

