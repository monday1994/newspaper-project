import React, { Component } from 'react';
import Router from './Router';

class App extends Component {
  render() {
    return (
        <Router style={{width: '100%', margin: '0 auto'}}/>
    );
  }
}

export default App;
