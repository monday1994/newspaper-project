export default {
    articles: {
        isFetching: false,
        list: [
            {
                id: 1,
                title: 'Lorem ipsum',
                content: 'Lorem ipsum dolor',
                images: {
                    order: 1,
                    url: 'http://temp-url.com/articles/images/img1'
                },
                author: {
                    id: 1,
                    name: 'Thomas',
                    lastname: 'Monday',
                    email: 'thoms@monday.com'
                }
            }
        ],
    },
    authors: {
        isFetching: false,
        list: [
            {
                id: 1,
                name: 'Thomas',
                lastname: 'Monday',
                email: 'thoms@monday.com'
            }
        ]
    }
}