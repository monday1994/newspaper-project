import { fetch } from '../api/articles';
import ACTIONS_TYPES from './action-types';
const { FETCH_ARTICLES } = ACTIONS_TYPES;

export const fetchArticles = () => {
    return async dispatch => {
        const articles = await fetch();
        dispatch({
            type: FETCH_ARTICLES,
            payload: articles
        });
    }
};