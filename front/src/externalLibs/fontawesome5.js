import { library } from '@fortawesome/fontawesome-svg-core';
import { faEnvelope, faKey, faSearch, faList } from '@fortawesome/free-solid-svg-icons';

library.add(faEnvelope, faKey, faSearch, faList);