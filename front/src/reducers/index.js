import articles from './articles';
import { combineReducers } from 'redux';

// here you defined what will be taken from store and pass to reducer (for example articles array NOT FULL STATE)
export default combineReducers({
    articles
});