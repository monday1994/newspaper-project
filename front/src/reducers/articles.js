import ACTIONS_TYPES from '../actions/action-types';
const { FETCH_ARTICLES } = ACTIONS_TYPES;

export default (state = {}, {type, payload}) => {
    const { list } = state;
    switch(type){
        case FETCH_ARTICLES:
            return {
                isFetching: false,
                list: [...list, ...payload]
            };
        default:
            return state;
    }
}